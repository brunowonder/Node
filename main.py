#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__    = 'Bruno Silva'
__copyright__ = 'Ninja Dynamics'
__license__   = "GPLv3"


import sys
import json
import thread
from time import time, sleep
from server import Server

# Get the command line args
# Example: port=31337, gate=192.168.0.1:31337
args = {}
for arg in sys.argv:
    kv = arg.split('=')
    if len(kv) < 2:
        continue
    k = kv[0]
    v = kv[1]
    for c in '\'"-, ':
        k = k.replace(c, '')
        v = v.replace(c, '')
    k = k.lower()
    v = v.lower()
    args[k] = v

# Start the server thread
thisNode = Server(args)
thread.start_new_thread(
    thisNode.start, ()
)

# Error checking
while not thisNode.status:
    pass
if thisNode.status != 'OK':
    print thisNode.status
    sys.exit(1)

# Display startup message
print '============================='
print 'This node is now LIVE!'
print '[ADDRESS]', thisNode.me
print '[GATEWAY]', thisNode.gate
print '-----------------------------'
print 'Type \'!EXIT\' to terminate.'
print '============================='
print

# Keep connections alive by constantly
# trying to find new peers whenever necessary
thread.start_new_thread(
    thisNode.keepTheNetworkAlive,
    (5, 10) # Interval in seconds (ping, pubkey)
)

# Keep this node alive
try:
    while True:
        cmd = raw_input().lower()
        if cmd == 'exit':
             break
        elif cmd == 'debug':
            thisNode.debug = not thisNode.debug
            print 'Debug mode:', thisNode.debug
except KeyboardInterrupt:
    pass

# Adios muchachos!
print '\nBye bye! :)'
