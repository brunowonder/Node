#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__    = 'Bruno Silva'
__copyright__ = 'Ninja Dynamics'
__license__   = "GPLv3"


import os
import re
import sys
import requests
from time import time
from binascii import b2a_base64
from Crypto.PublicKey import RSA

def showHelp():
    print '-- HELP ---------------------------------'
    print 'All messages are PUBLIC by default'
    print
    print 'Show public key indexes:'
    print '    @'
    print 'Single PRIVATE message:'
    print '    @node_index Your secret message'
    print 'Select node for PRIVATE messaging:'
    print '    @node_index'
    print 'Revert back to public broadcasting:'
    print '    ~!'
    print 'Help:'
    print '    ~?'
    print '-----------------------------------------'
    print

def showNodes():
    print '-- INFO ---------------------------------'
    print 'Available public keys:'
    for i, dct in enumerate(publicKeys):
        print '    [{}] {}'.format(i, dct['node'])
    print '-----------------------------------------'
    print

try:
    prompt   = 'Broadcast'
    gate     = '127.0.0.1:31337' if not len(sys.argv) > 1 else sys.argv[1]
    origin   = raw_input('Enter your name: ')

    while True:
        # Read / update public keys
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        publicKeys = []
        for filename in os.listdir('.'):
            if not filename.endswith('.pem'):
                continue
            with open(filename) as keyFile:
                publicKeys.append({
                    'node' : filename[:-4],
                    'key'  : RSA.importKey(keyFile.read())
                })
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Get user input
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        key       = None if prompt == 'Broadcast' else key
        data      = raw_input('{}> '.format(prompt))
        timestamp = str(time())
        dataType  = 'public'
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Determine the kind of input (public / private)
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if data.startswith('@'):
            try:
                matchExp = '^\@(\d+)(\s+|$)'
                matchObj = re.match(matchExp, data)
                if matchObj:
                    try:
                        idx  = int(matchObj.groups()[0])
                        msg  = len(matchObj.group())
                        key  = publicKeys[idx]['key']
                        data = data[msg:]
                        if not data:
                            prompt = '@{}'.format(
                                publicKeys[idx]['node'])
                            continue
                    except IndexError:
                        raise ValueError
                else:
                    raise ValueError
            except ValueError:
                showNodes()
                continue
        elif data == '~!':
            prompt = 'Broadcast'
            continue
        elif data == '~?':
            showHelp()
            continue

        if key:
            dataType = 'private'
            data = b2a_base64(key.encrypt(data, 'x')[0])
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Broadcast message
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        try:
            requests.post(
                'http://{}/broadcast'.format(gate),
                json={
                    "timestamp" : timestamp,
                    "origin"    : origin,
                    "data"      : data,
                    "type"      : dataType
                }
            )
        except requests.exceptions.ConnectionError:
            print '[Error] Gate not found / connection lost!'
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

except KeyboardInterrupt:
    print
    sys.exit(0)
