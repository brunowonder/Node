#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__    = 'Bruno Silva'
__copyright__ = 'Ninja Dynamics'
__license__   = "GPLv3"


import json
import string
import socket
from node import Node
from flask import Flask
from flask import request
from binascii import a2b_base64
from Crypto.Hash.SHA256 import SHA256Hash

import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app = Flask(__name__)
parent = None

class Server(Node):
    def __init__(self, args):
        super(Server, self).__init__(**args)
        global parent; parent = self
        parent.ignoreList = []
        self.status = None


    @app.route('/pubkey')
    def _pubkey():
        return parent.pubkey


    @app.route('/ping')
    def _ping():
        return 'pong'


    @app.route('/ipaddress')
    def _ipaddress():
        return parent.getIPAddress()


    @app.route('/nodes', methods=['GET','POST'])
    def _nodes():
        if request.method == 'POST':
            parent.addNewNodes(newNodes=request.get_json())
        return json.dumps(parent.nodes)


    @app.route('/broadcast', methods=['POST'])
    def _broadcast():
        payload = request.get_json()
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        msgSignature = SHA256Hash(
            json.dumps(payload)
        ).hexdigest()
        if msgSignature in parent.ignoreList:
            return 'done'
        parent.ignoreList.append(msgSignature)
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        origin   = payload['origin']
        data     = payload['data']
        dataType = payload['type']
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if dataType == 'pubkey':
            parent.networkPubkeys[origin] = data
            parent.savePublicKey(data, origin.replace(':', 'x'))
            # print len(parent.networkPubkeys)
        elif dataType == 'private':
            if parent.debug:
                 print '[base64] [{}] {}'.format(origin, data)
            try:
                data = parent.keypair.decrypt(
                    a2b_base64(data)
                )
                if all(c in string.printable for c in data):
                    print '[secure] [{}] {}'.format(origin, data)
            except (ValueError, TypeError):
                pass
        elif dataType == 'public':
                print '[public] [{}] {}'.format(origin, data)
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        parent.broadcast(payload)
        return 'ok'


    def start(self):
        try:
            res, tm = self.ping(self.me)
            if res != 'pong':
                self.status = 'OK'
                app.run(host='0.0.0.0', port=int(self.port))
            else:
                self.status = 'This address is already in use ({}).'.format(
                    self.me
                )
        except (socket.error, OverflowError, ValueError):
            self.status = 'Connection error.'
