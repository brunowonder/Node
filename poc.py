#!/usr/bin/env python

import json
dct = json.load(open('test.json'))


out = {}
stack = [raw_input('>').upper()]
while stack:
    # print stack
    # a = raw_input()
    x = stack[-1]
    out[x] = len(dct[x])           # Get this node's number of connections
    for i in dct[x]:               # Access each connection
        if i in out: continue      # If already known, get to the next one
        stack.append(i)            # [control] ----->
        break
    else:
        stack.pop()                # [control] <-----

print json.dumps(out, indent=4, sort_keys=True)

tuo = {}
minValue = 999
for k, v in out.iteritems():
    tuo[v] = k
    minValue = min(minValue, v)

print 'Selected node:', tuo[minValue]
