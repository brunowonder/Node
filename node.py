#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__    = 'Bruno Silva'
__copyright__ = 'Ninja Dynamics'
__license__   = "GPLv3"


import re
import os
import json
import thread
import socket
import requests
import pyqrcode

from time import time, sleep
from uuid import getnode as macAddr
from requests.exceptions import ConnectionError, InvalidURL

from random import shuffle
from Crypto.Hash.SHA256 import SHA256Hash
from Crypto.PublicKey import RSA
from Crypto import Random

class Node(object):
    def __init__(self, port=31337, gate=None, force=False):
        self.nodes          = []
        self.addressBook    = []
        self.ip             = self.getIPAddress()
        self.port           = str(port)
        self.me             = self.ip + ':' + self.port
        self.id             = SHA256Hash(self.me + str(macAddr())).hexdigest()
        self.QRCode         = pyqrcode.create(self.id).terminal(quiet_zone=1)
        self.gate           = self.validateGate(gate)
        self.force          = bool(force.lower() != 'false') if force else False
        self.pkTimestamp    = -1
        self.idealNumPeers  = 5
        self.debug          = False
        self.genKeypair()


    def genKeypair(self):
        KEY_LENGTH = 2048 # Key size (in bits)
        random_gen = Random.new().read
        self.keypair = RSA.generate(KEY_LENGTH, random_gen)
        self.pubkey = self.keypair.publickey().exportKey()
        self.networkPubkeys = { self.me: self.pubkey }
        self.savePublicKey(self.pubkey, self.me.replace(':', 'x'))


    def updateAddressBook(self, nodeAddress):
        before = list(sorted(self.addressBook))
        addressBookFilename = 'address-book.{}.txt'.format(
            self.me.replace(':', 'x')
        )
        if os.path.isfile(addressBookFilename):
            with open(addressBookFilename) as abFile:
                self.addressBook = [
                    line.strip() for line in abFile.readlines()
                ]
        self.addressBook = sorted(
            filter(
                lambda x: x != self.me,
                list(set(self.addressBook + [nodeAddress]))
            )
        )
        if self.addressBook == before:
            return
        with open(addressBookFilename, 'w') as abFile:
            for address in self.addressBook:
                abFile.write('{}\n'.format(address))


    def validateGate(self, nodeAddress, suppressWarnings=False):
        if not nodeAddress:
            return None
        try:
            ipMatch = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
            if re.match(ipMatch, nodeAddress):
                self.updateAddressBook(nodeAddress)
            else:
                raise ValueError
            return ':'.join([
                self.getIPAddress(nodeAddress),
                self.getPort(nodeAddress)])
        except ConnectionError:
            print '[WARNING] Gate not found: {}'.format(nodeAddress)
            return None
        except ValueError:
            print '[ERROR] Bad format: {}'.format(nodeAddress)
            return None


    def getIPAddress(self, nodeAddress=None):
        if nodeAddress:
            nodeAddress = self.safeAddress(nodeAddress)
            res = requests.get(nodeAddress + '/ipaddress').text
            return res
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
        s.close()
        return ip


    def getPort(self, nodeAddress):
        return nodeAddress.split(':')[-1].strip()


    def safeAddress(self, nodeAddress):
        if not nodeAddress.startswith('http://'):
            nodeAddress = 'http://{}'.format(nodeAddress)
        return nodeAddress


    def addNewNodes(self, sourceAddress=None, newNodes=None):
        if not newNodes:
            if not sourceAddress:
                return
            newNodes = self.requestNodes(sourceAddress)
        oldNodes = list(set(self.nodes))
        self.nodes = list(set(oldNodes + newNodes))
        if self.nodes != oldNodes:
            self.showFriends()


    def sendMyAddress(self, destAddress=None):
        if not destAddress:
            if not self.gate:
                return
            destAddress = self.gate
        destAddress = self.safeAddress(destAddress)
        # print 'Sending my address to: ' + destAddress
        # print
        res = requests.post(destAddress + '/nodes', json=[self.me]).text
        return res


    def ping(self, destAddress):
        destAddress = self.safeAddress(destAddress)
        ts = time()
        try:
            res = requests.get(destAddress + '/ping').text
            return res, time() - ts
        except (ConnectionError, InvalidURL):
            return None, time() - ts


    def requestNodes(self, nodeAddress):
        nodeAddress = self.safeAddress(nodeAddress)
        res = requests.get(nodeAddress + '/nodes').json()
        return res


    def post(self, nodeAddress, payload):
        try:
            requests.post(nodeAddress + '/broadcast', json=payload)
        except ConnectionError:
            pass


    def broadcast(self, payload):
        for destAddress in self.nodes:
            destAddress = self.safeAddress(destAddress)
            thread.start_new_thread(self.post, (destAddress, payload))


    def ask(self, msg):
        if isinstance(msg, list):
            msg = '\n'.join(msg)
        for i, c in enumerate(raw_input(msg)):
            if c != 'yes'[i]:
                return False
        return True


    def connectToNode(self, nodeAddress):
        self.sendMyAddress(nodeAddress)
        self.addNewNodes(newNodes=[nodeAddress])


    def broadcastPublicKey(self):
        payload = {
            'timestamp' : str(time()),
            'origin'    : self.me,
            'data'      : self.pubkey,
            'type'      : 'pubkey'
        }
        self.broadcast(payload)


    def testNode(self, nodeAddress):
        try:
            res, tm = self.ping(nodeAddress)
            if res != 'pong':
                raise ValueError
            self.updateAddressBook(nodeAddress)
            return True
        except (ConnectionError, ValueError):
            pass
        return False


    def findPeers(self):
        shuffle(self.addressBook)
        for node in self.addressBook:
            self.gate = self.validateGate(
                node, suppressWarnings=True)
            if self.gate:
                break
        else:
            return
        #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        question = [
            '[WARNING] This gate has no connections!',
            'Are you sure you want to connect (y/n)? '
        ]
        candidates = {}
        stack = [self.gate]
        while stack and len(self.nodes) < self.idealNumPeers:
            nodeAddr = stack[-1]
            # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
            try:
                lst = self.requestNodes(nodeAddr)
                if type(lst) != list:
                    raise ValueError
            except (ConnectionError, ValueError):
                stack.pop()
                continue
            lenLst = len(lst)
            if lenLst <= self.idealNumPeers:
                if lst == [] and not self.force\
                and self.ask(question) == False:
                    return
                self.connectToNode(nodeAddr)
            # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
            candidates[nodeAddr] = lenLst
            for entry in lst:
                if entry != self.me\
                and entry not in self.nodes\
                and entry not in candidates:
                    stack.append(entry)
                    break
            else:
                stack.pop()
            #print stack

        # Get the node with the lowest number of connections
        candidates = sorted(candidates.iteritems(), key=lambda (k, v): (v, k))
        for candidate, numConnections in candidates:
            if candidate in self.nodes:
                continue
            self.connectToNode(candidate)
            if len(self.nodes) >= self.idealNumPeers:
                break
        #print 'Done! :)'


    def keepTheNetworkAlive(self, pingInterval, pubkeyInterval):
        while True:
            for i, node in enumerate(self.nodes):
                if not self.testNode(node):
                    del self.nodes[i]
                    print '[WARNING] Connection lost:', node
                    self.showFriends()
            # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
            if len(self.nodes) < self.idealNumPeers:
                self.findPeers()
            if time() - self.pkTimestamp > pubkeyInterval:
                self.broadcastPublicKey()
                self.pkTimestamp = time()
            sleep(pingInterval)


    def savePublicKey(self, keyText, filename):
        with open(filename + '.pem','w') as keyFile:
            keyFile.write(keyText)


    def showFriends(self):
        print '-=' * 20
        print 'This node:', self.me
        print '-=' * 20
        print json.dumps(self.nodes, indent=4)
        print '-=' * 20
        print
        print
